
/***
 * My name is Shrey Dhungana
 * I write good codes
 ***/
#include "apue.h"
#include <dirent.h>
#include <unistd.h>
#include <stdio.h>



int
main(int argc, char *argv[])

{

    DIR           *dp;     //creating pointer for DIR
    struct dirent *dirp;   //creating ponter for stucture dirent
    char currentDir[1024]; //variable to access file
    int i;
    char *array[80];          //pointer to an arrayay to store the data

    if (getcwd(currentDir, sizeof(currentDir)) != NULL){    //if  the file is not empty
        dp = opendir(currentDir);                        //opens the directory


        if (argc ==1){

            while((dirp=readdir(dp))!=NULL)

                if(strcmp(dirp->d_name, ".") && strcmp(dirp->d_name, "..")){   	//not printing the "." and ".." files
                    printf("%s \n", dirp->d_name);                               // prints the result same as "ls"
                }

        }
        //if the second argument is "-a", show "." and ".." files too
        else if(strcmp(argv[1], "-a") == 0){         //chekcs for the "-a" flag
            while((dirp=readdir(dp))!=NULL)            //when the checked directory is not empty
                printf("%s \n", dirp->d_name);           // prints the "." and ".." files also

        }
    }    
    else{  //if the file path is not found
        printf("Path not found.Error!");
    }

    closedir(dp); //close the directory
    return 0;//close the program
}
